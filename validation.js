const Joi = require("@hapi/joi");

//Create Validation
const createValidation = (data) => {
  const schema = Joi.object({
    username: Joi.string()
      .min(6)
      .required(),
    email: Joi.string()
      .min(6)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .required()
  });

  return schema.validate(data);
};

//Login Validation
const loginValidation = (data) => {
  const schema = Joi.object({
    username: Joi.string()
      .alphanum()
      .min(6)
      .required(),
    password: Joi.string()
      .min(6)
      .required()
  });

  return schema.validate(data);
};

//Journal Validation
const journalValidation = (data) => {
  const schema = Joi.object({
    title: Joi.string()
      .min(1)
      .max(255)
      .required(),
    message: Joi.string()
      .min(6)
      .max(1024)
      .required(),
    imgUrl: Joi.string()
      .min(0)
      .max(1024),
    ownerId: Joi.string()
      .min(4)
      .max(10)
      .required()
  });
  return schema.journalValidation(data);
};

//Journal Validation
const journalUpdateValidation = (data) => {
  const schema = Joi.object({
    title: Joi.string()
      .min(1)
      .max(255)
      .required(),
    message: Joi.string()
      .min(6)
      .max(1024)
      .required(),
    imgUrl: Joi.string()
      .min(0)
      .max(1024)
  });
  return schema.journalUpdateValidation(data);
};
module.exports.createValidation = createValidation;
module.exports.loginValidation = loginValidation;
module.exports.journalValidation = journalValidation;
module.exports.journalUpdateValidation = journalUpdateValidation;
