const mongoose = require("mongoose");

const followSchema = new mongoose.Schema({
  following: Array
});

module.exports = mongoose.model("Follow", followSchema);
