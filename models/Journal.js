const mongoose = require("mongoose");

const journalSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    min: 1,
    max: 255
  },
  message: {
    type: String,
    required: true,
    max: 1024,
    min: 6
  },
  imageUrl: {
    type: String,
    max: 1024,
    min: 6
  },
  ownerId: {
    type: String,
    required: true,
    max: 10,
    min: 4
  },
  createDate: {
    type: Date,
    default: Date.now
  },
  updateDate: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Journal", journalSchema);
