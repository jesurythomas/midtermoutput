const express = require("express");
const router = express.Router();
const User = require("../models/User");
const jwt = require("jsonwebtoken");
const Journal = require("../models/Journal");
const verify = require("./verifyToken");

router.post("/:userId", verify, async (req, res) => {
  var thisUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
  const user = await User.findById({ _id: thisUser._id });

  const follow = await User.findOne({
    _id: req.params.userId
  });
  if (!follow) return res.status(404).send("User does not exist");

  console.log(user._id);
  console.log(req.params.userId);
  try {
    User.findByIdAndUpdate(
      user._id,
      { $addToSet: { following: req.params.userId } },
      { safe: true, upsert: true },
      function(err, doc) {
        if (err) {
          res.status(404).send(err);
        } else {
          res.status(200).send("Successfully Followed");
        }
      }
    );
  } catch (err) {
    res.send(err);
  }
});

module.exports = router;
