const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("../models/User");
const { createValidation, loginValidation } = require("../validation");

//Create User

router.post("/create", async (req, res) => {
  // Validate Data
  const { error } = createValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  //Check if the user and email is already in the database
  const userExists = await User.findOne({
    username: req.body.username
  });
  if (userExists) return res.status(400).send("User already exists");

  const emailExists = await User.findOne({
    email: req.body.email
  });
  if (emailExists) return res.status(400).send("Email is already registered");

  //Hash password
  const salt = await bcrypt.genSalt(1);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  //Create a new user
  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: hashedPassword
  });
  try {
    await user.save();
    res.status(201).send("Account Successfully Created");
  } catch (err) {
    res.status(500).send(err);
  }
});

// Login

router.post("/login", async (req, res) => {
  // Validate Data
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Checks if the user exists
  const user = await User.findOne({
    username: req.body.username
  });
  if (!user) return res.status(404).send("User not found based unique username");
  // Check if password is correct
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(400).send("Invalid Password");

  //Create and assign a token
  try {
    const token = await jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, {
      expiresIn: 60 * 60 * 60
    });
    res.header("auth-token", token);
    res.write(token);
    res.status(200);
    res.write(" ------------ Signed in successfully");
    res.end();
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
