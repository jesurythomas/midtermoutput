const express = require("express");
const router = express.Router();
const verify = require("./verifyToken");
const Journal = require("../models/Journal");
const jwt = require("jsonwebtoken");
const User = require("../models/User");

router.post("/", verify, async (req, res) => {
  var thisUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
  const user = await User.findById({ _id: thisUser._id });

  const journal = new Journal({
    title: req.body.title,
    message: req.body.message,
    imgUrl: req.body.imgUrl,
    ownerId: user._id
  });

  try {
    await journal.save();
    res.status(201).send("Journal Entry Successfully Created");
  } catch (err) {
    res.status(400).send("Title, Message, and OwnerId must be supplied");
  }
});

// Update a journal
router.put("/:postId", verify, async (req, res) => {
  // Checks if the journal entry exists
  const journal = await Journal.findOne({
    _id: req.params.postId
  });
  if (!journal) return res.status(404).send("Journal entry does not exist");

  try {
    const updatePost = await Journal.updateOne(
      { _id: req.params.postId },
      {
        $set: {
          title: req.body.title,
          message: req.body.message,
          imgUrl: req.body.imgUrl,
          ownerId: journal.ownerId,
          updateDate: Date.now()
        }
      }
    );
    res.status(200).send("Post updated successfully");
  } catch (err) {
    res.status(400).send(err);
  }
});

router.delete("/:postId", verify, async (req, res) => {
  const journal = await Journal.findOne({
    _id: req.params.postId
  });
  if (!journal) return res.status(404).send("Journal entry does not exist");

  try {
    await Journal.remove({ _id: req.params.postId });
    res.status(200).send("Post removed successfully");
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
