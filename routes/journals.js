const express = require("express");
const router = express.Router(); // Creates route
const Journal = require("../models/Journal");
const verify = require("./verifyToken");
const User = require("../models/User");
const jwt = require("jsonwebtoken");

router.get("/", verify, async (req, res) => {
  var thisUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
  const user = await User.findById({ _id: thisUser._id });

  const journal = await Journal.find({ ownerId: user._id });
  var journalArray = new Array();

  journal.forEach((journalEntry) => {
    journalArray.push(journalEntry);
  });

  user.following.forEach(async (userId) => {
    followingPost = await Journal.find({ ownerId: userId });
    followingPost.forEach((journalEntry) => {
      journalArray.push(journalEntry);
    });
  });
  try {
    res.send(journalArray);
  } catch (err) {
    res.status(500).res(err);
  }
});

module.exports = router;
