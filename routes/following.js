const express = require("express");
const router = express.Router(); // Creates route
const Journal = require("../models/Journal");
const verify = require("./verifyToken");
const jwt = require("jsonwebtoken");
const User = require("../models/User");

router.get("/", verify, async (req, res) => {
  var thisUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
  const user = await User.findById({ _id: thisUser._id });

  try {
    res.json(user.following);
  } catch (err) {
    res.json(err);
  }
});

module.exports = router;
