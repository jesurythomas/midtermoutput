const jwt = require("jsonwebtoken");

module.exports = function(req, res, next) {
  const token = req.header("auth-token");
  if (!token) return res.status(403).send("Forbidden");

  var decoded = jwt.decode(token, { complete: true });
  console.log(decoded.header);
  console.log(decoded.payload);

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (err) {
    res.status(403).send("Forbidden");
  }
};
