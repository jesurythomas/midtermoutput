const express = require("express");
const router = express.Router(); // Creates route
const verify = require("./verifyToken");
const User = require("../models/User");

router.get("/", async (req, res) => {
  try {
    const user = await User.find();
    res.json(user);
  } catch (err) {
    res.json({ message: err });
  }
});

router.get("/:username", verify, async (req, res) => {
  var getSearch = req.params.username;
  try {
    const user = await User.find({ username: { $regex: getSearch, $options: "i" } });
    res.json(user);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
