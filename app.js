const express = require("express"); // Import package
const app = express(); // Execute package
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
require("dotenv/config");

//Middleware - function that executes when routes are being hit
app.use(bodyParser.json());
app.use(express.json());

//Import Routes
const unfollowRoute = require("./routes/unfollow");
const followRoute = require("./routes/follow");
const postsRoute = require("./routes/posts");
const authRoute = require("./routes/auth");
const userRoute = require("./routes/user");
const journalRoute = require("./routes/journal");
const journalsRoute = require("./routes/journals");
const followingRoute = require("./routes/following");
// Middlewares routes
app.use("/following", followingRoute);
app.use("/posts", postsRoute);
app.use("/user", authRoute);
app.use("/users", userRoute);
app.use("/journal", journalRoute);
app.use("/journals", journalsRoute);
app.use("/follow", followRoute);
app.use("/unfollow", unfollowRoute);

//Connect to DB
mongoose.connect(
  process.env.DB_CONNECTION,
  { useUnifiedTopology: true, useNewUrlParser: true },
  () => console.log("Connected to DB!")
);

//Start Listening to the server
app.listen(3000, () => console.log("Server is up and running"));
